## O desafio

Neste desafio você terá interpretar uma prescrição médica (vinda de uma rest api no formato JSON), dar opções de escolha de horários para o user baseado nas instruções do cronograma da prescrição e criar alarmes que disparam uma Local Notification em cada vez que precisar tomar o medicamento. Nosso principal objetivo é conhecer como você aborda os problemas e desenvolve soluções.

O App basicamente é uma mini versão da integração com a plataforma de prescrição Dr. CUCO, onde o mesmo capta as informações de uma API e converte em alarmes de medicamentos. A partir dos alarmes são geradas notificações locais para avisar o usuário de tomar a medicação.

Uma especificação básica segue abaixo. Tudo que não foi especificado deve ser decidido por você, isso também será avaliado.

## Especificação

A aplicação deve ter duas telas principais e um sistema de notificação de Medicamentos (Segue prints do iOS para se inspirar) :

* Dashboard (Home Screen): Lista com alguns medicamentos.
* Prescrição: tela onde sera mostrada a prescrição e onde o user configurará os horários dos medicamentos baseado na prescrição medica.
* Notificações: notificações na tela bloqueada do celular para lembrar o usuário de tomar o medicamento.

### Como se diferenciar dos demais?

Não é obrigatório, mas é um diferencial importante:

* Testes Automatizados (a intenção é escrever testes, não necessáriamente ter 100% de cobertura e testes passando)
* Permitir avisar se tomou ou não a medicação diretamente pela notificação...

## Inicio do desafio

Você deve iniciar na hora combinada por email e fazer um fork desse repositório. Quando terminar deve enviar um pull-request.

O horário do pull-request será o horário considerado para a entrega.

## Será avaliado

- Qualidade do codigo
- Estrutura de codigo e projeto
- Ferramentas e tecnologias usadas
- Diferenciais incluidos
- % de conclusão do projeto

## Recomendações finais

* Nossa avaliação não será apenas em cima de produtividade, daremos grande valor a qualidade do código escrito.
* Não iremos considerar entregas após o prazo combinado.
* Não tente burlar horários e prazos, essa é uma péssima maneira de começar e não iremos tolerar tal atitude.

Boa Sorte!

## Inspiração do app iOS

![Captura de Tela 2016-04-25 às 14.57.37.png]
(https://bitbucket.org/repo/yknnrd/images/1424886986-Captura%20de%20Tela%202016-04-25%20%C3%A0s%2014.57.37.png)

![Captura de Tela 2016-04-25 às 14.58.04.png](https://bitbucket.org/repo/yknnrd/images/1334347379-Captura%20de%20Tela%202016-04-25%20%C3%A0s%2014.58.04.png)

## JSON Prescrição Médica
[Prescriçao JSON](http://www.jsoneditoronline.org/?id=7bcbf7f55ad84fd72245887ec1792f7f)